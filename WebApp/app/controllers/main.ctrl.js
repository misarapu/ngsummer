﻿(function () {
    'use strict';

    angular.module('app').controller('MainCtrl', Ctrl);

    function Ctrl($window) {
        var vm = this;

        vm.relaodSite = reloadSite;

        function reloadSite() {
            $window.location.reload();
        }
    }
})();