﻿(function () {
    'use strict';
    
    angular.module('app').controller('SymptomsCtrl', Ctrl);
    Ctrl.$inject = ['SymptomsSrv','DiagnosisSrv'];

    function Ctrl(SymptomsSrv, DiagnosisSrv) {
        var vm = this;

        vm.count = 0;
        vm.diagnosis = [];
        vm.symptoms = [];
        vm.topSymptoms = [];
        vm.diagnosisMessage;

        vm.initLoadSymptoms = initLoadSymptoms;
        vm.getDiagnosis = getDiagnosis;
        vm.setProbabilityColor = setProbabilityColor;
        
        vm.getTopSymptoms = getTopSymptoms;

        function initLoadSymptoms() {
            loadSymptoms();
            getCountOfSymptoms();
        }
        
        function loadSymptoms() {
            SymptomsSrv.getAllSymptoms().then(function (response) {
                vm.symptoms = response.data;
            });
        }

        function getCountOfSymptoms() {
            SymptomsSrv.getCountOfSymptoms().then(function (response) {
                vm.count = response.data;
            });
        }

        function getDiagnosis() {
            //IE11 does not accept lambda expression
            var selected = vm.symptoms.filter(function (x) {
                return x.selected;
            }).map(function(x) {
                return x.name;
            });
            //var selected = vm.symptoms.filter(x => x.selected).map(x => x.name);
            DiagnosisSrv.getDiagnosis(selected).then(function (response) {
                setDiagnosisResult(response);
            });
        }

        function getTopSymptoms(count) {
            SymptomsSrv.getTopSymptoms(count).then(function (response) {
                vm.topSymptoms = response.data;
            });
        }

        function setProbabilityColor(probability) {
            if (probability >= 0.85)
                return 'danger';
            if (probability > 0.5 && probability < 0.85)
                return 'serious';
            if (probability <= 0.6)
                return 'mild';
        }

        function setDiagnosisResult(response) {
            vm.diagnosis = response.data;
            switch (response.status) {
                case 200:
                    vm.diagnosisMessage = '';
                    break;
                case 204:
                    vm.diagnosisMessage = 'There is no disease with such combination of symptoms in the system. Please contact your doctor!';
                    break;
                default:
                    break;
            }
        }
    }
})();