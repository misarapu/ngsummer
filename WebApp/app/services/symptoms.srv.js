﻿(function () {
    'use strict';

    angular.module('app').service('SymptomsSrv', Srv);

    function Srv($http) {
        return {
            getAllSymptoms: getAllSymptoms,
            getCountOfSymptoms: getCountOfSymptoms,
            getTopSymptoms: getTopSymptoms
        }

        function getAllSymptoms() {
            return $http.get('/api/symptoms');
        }

        function getCountOfSymptoms() {
            return $http.get('/api/symptoms/count');
        }

        function getTopSymptoms(count) {
            return $http.get('/api/symptoms', {
                params: {count: count}});
        }
    }
})();