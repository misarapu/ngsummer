﻿using BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WebApp.Helpers;

namespace WebApp.Controllers.Api
{
    public class DeseasesController : ApiController
    {
        private readonly DeseaseStatsService _service;

        public DeseasesController()
        {
            _service = new DiagnosisService(DataPaths.deseases);
        }

        [HttpGet]
        [Route("api/deseases/top")]
        public IHttpActionResult GetDeseasesBySymptomsCount([FromUri]int count)
        {
            var result = _service.DeseasesWithMostSymptoms(count);
            if (result.Count == 0)
            {
                return Content(HttpStatusCode.NoContent, result);
            }
            return Ok(result);
        }
    }
}
