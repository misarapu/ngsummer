﻿using BL.DTOs;
using BL.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Http;
using WebApp.Helpers;

namespace WebApi.app.controllers
{
    public class SymptomsController : ApiController
    {
        private readonly DiagnosisService _service;

        public SymptomsController()
        {
            _service = new DiagnosisService(DataPaths.deseases);
        }

        [HttpGet]
        [Route("api/symptoms")]
        public IHttpActionResult GetSymptoms()
        {
            var result = _service.AllSymptoms();
            if (result.Count == 0)
            {
                return Content(HttpStatusCode.NoContent, result);
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("api/symptoms/count")]
        public IHttpActionResult GetCountOfSymptoms()
        {
            return Ok(_service.CountOfSymptoms());
        }

        [HttpGet]
        [Route("api/symptoms")]
        public IHttpActionResult GetTopSymptoms([FromUri]int count)
        {
            var result = _service.MostCommonSymptoms(count);
            if (result.Count == 0)
            {
                return Content(HttpStatusCode.NoContent, result);
            }
            return Ok(result);
        }

    }
}
