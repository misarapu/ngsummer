﻿using BL.DTOs;
using BL.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebApp.Helpers;

namespace WebApp.Controllers.Api
{
    public class DiagnosisController : ApiController
    {
        private readonly DiagnosisService _service;

        public DiagnosisController()
        {
            _service = new DiagnosisService(DataPaths.deseases);
        }

        public IHttpActionResult Get([FromUri]string symptoms)
        {
            var list = JsonConvert.DeserializeObject<List<string>>(symptoms);
            var result = _service.GiveDiagnosis(list);
            if (result.Count == 0)
            {
                return Content(HttpStatusCode.NoContent, result);
            }
            return Ok(result);
        }

    }
}
