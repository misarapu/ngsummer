﻿(function () {
    'use strict';

    angular.module('app').service('DiagnosisSrv', Srv);

    function Srv($http) {
        return {
            getDiagnosis: getDiagnosis
        }

        function getDiagnosis(symptoms) {
            console.log(symptoms);
            return $http.get('/api/diagnosis', {
                params: { symptoms: JSON.stringify(symptoms) }
            });
        }
    }
})();