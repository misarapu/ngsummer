﻿(function () {
    'use strict';

    angular.module('app').controller('DeseasesCtrl', Ctrl);
    Ctrl.$inject = ['DeseasesSrv'];
    
    function Ctrl(DeseasesSrv) {
        var vm = this;

        vm.topDeseases = []

        getDeseasesBySymptomsCount(3);

        function getDeseasesBySymptomsCount(count) {
            DeseasesSrv.getDeseasesBySymptomsCount(count).then(function (response) {
                vm.topDeseases = response.data;
            });
        }
        
    }
})();