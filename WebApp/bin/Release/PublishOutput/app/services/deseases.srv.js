﻿(function () {
    'use strict';

    angular.module('app').service('DeseasesSrv', Srv);

    function Srv($http) {
        return {
            getDeseasesBySymptomsCount: getDeseasesBySymptomsCount
        }

        function getDeseasesBySymptomsCount(count) {
            return $http.get('/api/deseases/top', {
                params: {count: count}});
        }
    }
})();