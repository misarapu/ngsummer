﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteractiveConsole
{
    public class Commands
    {
        public static List<string> CommandsList = new List<string>()
        {
            "analyze",
            "diagnosis",
            "show stats",
            "exit"

        };

        public static void ShowCommands()
        {
            Console.WriteLine("Valid commands: ");
            foreach (var command in CommandsList)
            {
                Console.WriteLine($"** {command}");
            }
        }
    }
}
