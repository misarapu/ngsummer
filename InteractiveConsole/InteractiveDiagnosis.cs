﻿using BL.DTOs;
using BL.Services;
using BL.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InteractiveConsole.ViewModels;

namespace InteractiveConsole
{
    class InteractiveDiagnosis
    {
        static void Main(string[] args)
        {
            Graphics.Welcome();
            Console.WriteLine("Data path: ");
            var path = Console.ReadLine();

            string command = "";
            while (command != "exit")
            {
                Console.WriteLine("Command: ");
                command = Console.ReadLine();
                switch (command)
                {
                    case "analyze":
                        RunAnalyzer(path);
                        break;
                    case "diagnosis":
                        RunDiagnosis(path);
                        break;
                    case "show stats":
                        RunStatistics(path);
                        break;
                    case "exit":
                        Environment.Exit(-1);
                        break;
                    default:
                        Commands.ShowCommands();
                        break;
                }
            }
        }

        public static void RunStatistics(string path)
        {
            Graphics.TaskTitle("Statistics");
            var vm = new StatisticsVM(path);
            vm.ShowThreeDeseasesWithMostSymptoms();
            vm.ShowThreeMostCommonSymptoms();
            vm.ShowCountOfSymptoms();
        }

        public static void RunDiagnosis(string path)
        {
            Graphics.TaskTitle("Diagnosis");
            var vm = new DiagnosisVM(path);
            vm.AskSymptoms();
        }

        public static void RunAnalyzer(string path)
        {
            Graphics.TaskTitle("Analyzer");
            var vm = new AnalyzerVM(path);
            var result = vm.Analyze();

            if (result != null)
            {
                Console.WriteLine($"You have {result.Name}.");
            }
            else
            {
                Console.WriteLine($"No such desease in system.");
            }
        }


    }
}
