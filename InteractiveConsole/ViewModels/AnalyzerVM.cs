﻿using BL.DTOs;
using BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteractiveConsole.ViewModels
{
    public class AnalyzerVM
    {
        private InteractiveDiagnosisService _service;
        private List<DeseaseDTO> _deseases;
        private List<SymptomDTO> _symptoms;
        private List<string> _query;

        public AnalyzerVM(string dataPath)
        {
            _service = new InteractiveDiagnosisService(@dataPath);
            _deseases = _service.AllDeseases();
            _symptoms = _service.AllSymptoms();
            _query = new List<string>();
        }

        public DeseaseDTO Analyze()
        {
            var symptom = _symptoms.FirstOrDefault();
            // find the one desease
            while (_deseases.Count > 1)
            {
                AskAboutNewSymptom(symptom);
                _symptoms = _service.SymptomsThatAreNotInAllDeseases(_deseases);
                symptom = _symptoms.FirstOrDefault();
            }
            return _deseases.FirstOrDefault();
        }

        private void AskAboutNewSymptom(SymptomDTO symptom)
        {
            Console.Write($"Do you have {symptom.Name}? (y/n) ");
            var answer = Console.ReadLine();

            if (answer.ToLower() == "y") PositiveAnswer(symptom);
            else if (answer.ToLower() == "n") NegativeAnswer(symptom);
            else InvalideAnswer();
        }

        private void PositiveAnswer(SymptomDTO symptom)
        {
            _query.Add(symptom.Name);
            // remove all desease which symptoms don't contain query symptoms
            _deseases.RemoveAll(x => _service.CalculateMatch(x, _query) == 0);
        }

        private void NegativeAnswer(SymptomDTO symptom)
        {
            // remove all deseases which have symptoms that aren't in query
            _deseases.RemoveAll(x => x.Symptoms.Select(s => s.Name).Contains(symptom.Name));
        }

        private void InvalideAnswer()
        {
            Console.WriteLine("Invalid answer.");
        }

    }
}
