﻿using BL.DTOs;
using BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteractiveConsole.ViewModels
{
    public class DiagnosisVM
    {
        private DiagnosisService _service;
        private readonly List<SymptomDTO> _dbSymptoms;
        private List<string> _query;

        public DiagnosisVM(string dataPath)
        {
            _service = new DiagnosisService(dataPath);
            _dbSymptoms = _service.AllSymptoms();
            _query = new List<string>();
        }

        public void AskSymptoms()
        {
            while (true)
            {
                Console.WriteLine("Symptom: ");
                var symptom = Console.ReadLine();
                if (_dbSymptoms.Where(x => x.Name == symptom.ToLower()).Any())
                {
                    _query.Add(symptom);
                    ShowResults();
                }
                else
                {
                    break;
                }
            }
        }

        public void ShowResults()
        {
            var diagnosis = _service.GiveDiagnosis(_query);
            foreach (var res in diagnosis)
            {
                Console.WriteLine($"{res.Name} ({res.Probability*100})");
            }
        }

    }
}
