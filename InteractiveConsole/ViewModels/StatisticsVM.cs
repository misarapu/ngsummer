﻿using BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteractiveConsole.ViewModels
{
    public class StatisticsVM
    {
        private DiagnosisService _service;
        public StatisticsVM(string dataPath)
        {
            _service = new DiagnosisService(dataPath);
        }

        public void ShowThreeDeseasesWithMostSymptoms()
        {
            Console.WriteLine("Deseases with most symptoms: ");
            foreach (var desease in _service.DeseasesWithMostSymptoms(3))
            {
                Console.WriteLine($"\t**{desease.Name}: ");
                foreach (var symptom in desease.Symptoms)
                {
                    Console.WriteLine($"\t\t{symptom.Name}");
                }
            }
        }

        public void ShowCountOfSymptoms()
        {
            Console.WriteLine($"Total count of symptoms: {_service.CountOfSymptoms()}");
        }

        public void ShowThreeMostCommonSymptoms()
        {
            Console.WriteLine("Most common symptoms: ");
            foreach (var symptom in _service.MostCommonSymptoms(3))
            {
                Console.WriteLine($"\t*{symptom.Name}");
            }
        }
    }
}
