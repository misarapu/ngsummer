﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteractiveConsole
{
    public class Graphics
    {
        public static void Welcome()
        {
            Console.WriteLine("********************************************************");
            Console.WriteLine("*                    WHAT'S YOU GOT                    *");
            Console.WriteLine("********************************************************");
        }

        public static void TaskTitle(string title)
        {
            Console.WriteLine();
            Console.WriteLine($"\t{title}");
            Console.WriteLine("========================================================");
        }
    }
}
