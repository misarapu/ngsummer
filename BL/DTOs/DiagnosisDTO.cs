﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTOs
{
    public class DiagnosisDTO : DeseaseDTO
    {
        public int Match { get; set; }
        public double Probability
        {
            get
            {
                return (double) Match/Symptoms.Count();
            }
        }
    }
}
