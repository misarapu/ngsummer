﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTOs
{
    public class DeseaseDTO
    {
        public string Name { get; set; }
        public List<SymptomDTO> Symptoms { get; set; }
    }
}
