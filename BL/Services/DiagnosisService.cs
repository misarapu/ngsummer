﻿using System;
using System.Collections.Generic;
using System.Linq;
using BL.Helpers;
using System.Text;
using System.Threading.Tasks;
using BL.DTOs;

namespace BL.Services
{
    public class DiagnosisService : DeseaseStatsService
    {
        public DiagnosisService(string dataPath) : base(dataPath)
        {
        }

        public List<DiagnosisDTO> GiveDiagnosis(List<string> queryList)
        {
            var diagnosisList = new List<DiagnosisDTO>();
            foreach (var desease in _deseases)
            {
                var match = CalculateMatch(desease, queryList);
                if (match != 0)
                {
                    diagnosisList.Add(new DiagnosisDTO
                    {
                        Name = desease.Name,
                        Symptoms = desease.Symptoms,
                        Match = match
                    });
                }
            }
            return diagnosisList.OrderByDescending(x => x.Probability).ToList();
        }

        public int CalculateMatch(DeseaseDTO desease, List<string> queryList)
        {
            var match = desease.Symptoms
                .Select(s => s.Name)
                .ComapareLists(queryList);
            return queryList.Count() == match ? match : 0;
        }
    }
}
