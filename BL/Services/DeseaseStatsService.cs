﻿using BL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using BL.Helpers;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
    public class DeseaseStatsService
    {
        protected IEnumerable<DeseaseDTO> _deseases;

        public DeseaseStatsService(string dataPath)
        {
            _deseases = DataProvider.LoadDeseases(@dataPath)
                    .OrderByDescending(d => d.Symptoms.Count())
                    .GroupBy(d => d.Symptoms.Count())
                    .Select(grp => grp.OrderBy(d => d.Name))
                    .SelectMany(grp => grp);
        }

        public List<DeseaseDTO> AllDeseases()
        {
            return _deseases.ToList();
        }

        public List<SymptomDTO> AllSymptoms()
        {
            return _deseases.SelectMany(d => d.Symptoms)
                            .DistinctBy(s => s.Name)
                            .OrderBy(s => s.Name)
                            .ToList();
        }

        public List<DeseaseDTO> DeseasesWithMostSymptoms(int listLength)
        {
            return _deseases.Take(listLength)
                            .ToList();
        }

        public int CountOfSymptoms()
        {
            return AllSymptoms().Count();
        }

        public List<SymptomDTO> MostCommonSymptoms(int listLength)
        {
            return _deseases.SelectMany(d => d.Symptoms)
                            .OrderBy(s => s.Name)
                            .GroupBy(s => s.Name)
                            .OrderByDescending(grp => grp.Count())
                            .Select(grp => grp.First())
                            .Take(listLength)
                            .ToList();
        }
    }
}
