﻿using BL.DTOs;
using BL.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
    public class InteractiveDiagnosisService : DiagnosisService
    {
        public InteractiveDiagnosisService(string dataPath) : base(dataPath)
        {
        }

        public List<SymptomDTO> SymptomsThatAreNotInAllDeseases
            (List<DeseaseDTO> deseases)
        {
            return deseases.SelectMany(x => x.Symptoms)
                        .GroupBy(s => s.Name)
                        .Where(grp => grp.Count() != deseases.Count())
                        .SelectMany(grp => grp)
                        .DistinctBy(s => s.Name)
                        .ToList();
        }

    }
}
