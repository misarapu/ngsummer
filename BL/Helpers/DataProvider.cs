﻿using BL.DTOs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Helpers
{
    public class DataProvider
    {
        public static IEnumerable<DeseaseDTO> LoadDeseases(string path)
        {
            return File.ReadAllLines(path, Encoding.Default)
                .Select(x => x.Split(','))
                .Select(x => new DeseaseDTO
                {
                    Name = x.ElementAt(0).ToString().Trim(),
                    Symptoms = x.Skip(1).Select(y => new SymptomDTO
                    {
                        Name = y.ToString().Trim()
                    })
                    .ToList()
                });
        }
    }
}
