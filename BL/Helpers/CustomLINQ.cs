﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Helpers
{
    public static class CustomLINQ
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
            (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }

        public static int ComapareLists<TElement>
            (this IEnumerable<TElement> dataList, List<TElement> queryList)
        {
            var count = 0;
            foreach (var query in queryList)
            {
                if (dataList.Contains(query)) count++;
            }
            return count;
        }
    }
}
