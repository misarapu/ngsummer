﻿using BL.Helpers;
using BL.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGProject
{
    class TestConsole
    {
        static void Main(string[] args)
        {
            var context = new DiagnosisService(@"testData.csv");
            var patient = new List<string>()
            {
                "photosensitivity", "zombie-like behavior","obsessive compulsive behavior"
            };

            ShowThreeDeseasesWithMostSymptoms(context);
            ShowCountOfSymptoms(context);
            ShowThreeMostCommonSymptoms(context);
            ShowDiagnosis(context, patient);
        }

        public static void ShowThreeDeseasesWithMostSymptoms(DiagnosisService context)
        {
            Console.WriteLine("Deseases with most symptoms: ");
            foreach (var desease in context.DeseasesWithMostSymptoms(3))
            {
                Console.WriteLine($"\t**{desease.Name}: ");
                foreach (var symptom in desease.Symptoms)
                {
                    Console.WriteLine($"\t\t{symptom.Name}");
                }
            }
        }

        public static void ShowCountOfSymptoms(DiagnosisService context)
        {
            Console.WriteLine($"Total count of symptoms: {context.CountOfSymptoms()}");
        }

        public static void ShowThreeMostCommonSymptoms(DiagnosisService context)
        {
            Console.WriteLine("Most common symptoms: ");
            foreach (var symptom in context.MostCommonSymptoms(3))
            {
                Console.WriteLine($"\t*{symptom.Name}");
            }
        }

        public static void ShowDiagnosis(DiagnosisService context, List<string> symptoms)
        {
            Console.WriteLine("You might have:");
            foreach (var d in context.GiveDiagnosis(symptoms))
            {
                Console.WriteLine($"\t*{d.Name}({d.Probability.ToString("P")})");
            }
        }
    }
}
